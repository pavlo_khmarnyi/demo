package com.example.demo.repository;

import com.example.demo.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    List<Book> findAll();
    Book findBookById(Long id);
    Book save(Book book);
    void delete(Book book);
}
