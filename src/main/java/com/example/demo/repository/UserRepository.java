package com.example.demo.repository;

import com.example.demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findAll();
    User findUserById(Long id);
    User findUserByFirstNameAndLastName(String firstName, String lastName);
    User save(User user);
    void delete(User user);
}
