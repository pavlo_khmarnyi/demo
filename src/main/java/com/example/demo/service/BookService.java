package com.example.demo.service;

import com.example.demo.entity.Book;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
public interface BookService {
    List<Book> getAllBooks();
    Book getBookById(Long id);
    Long createBook(Book book);
    Long deleteBookById(Long id);
}
