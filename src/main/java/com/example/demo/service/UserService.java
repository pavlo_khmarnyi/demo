package com.example.demo.service;

import com.example.demo.entity.User;
import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
public interface UserService {
    List<User> getAllUsers();
    User getUserById(Long id);
    User getUserByName(String firstName, String lastName);
    Long createUser(User user);
    User updateUser(User user, Long id);
    Long deleteUserById(Long id);
}
