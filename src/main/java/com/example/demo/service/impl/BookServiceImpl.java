package com.example.demo.service.impl;

import com.example.demo.entity.Book;
import com.example.demo.repository.BookRepository;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
@Service
public class BookServiceImpl implements BookService {

    @Autowired
    private BookRepository repository;

    @Override
    public List<Book> getAllBooks() {
        List<Book> books = repository.findAll();
        return books;
    }

    @Override
    public Book getBookById(Long id) {
        Book book = repository.findBookById(id);
        return book;
    }

    @Override
    public Long createBook(Book book) {
        Book savedBook = repository.save(book);
        return savedBook.getId();
    }

    @Override
    public Long deleteBookById(Long id) {
        Book book = repository.findBookById(id);
        if (book != null) {
            repository.delete(book);
            return id;
        }
        return null;
    }
}
