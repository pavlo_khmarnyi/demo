package com.example.demo.service.impl;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavlo Khmarnyi
 * @since 11/10/2019
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Override
    public List<User> getAllUsers() {
        List<User> users = repository.findAll();
        return users;
    }

    @Override
    public User getUserById(Long id) {
        User user = repository.findUserById(id);
        return user;
    }

    @Override
    public User getUserByName(String firstName, String lastName) {
        User user = repository.findUserByFirstNameAndLastName(firstName, lastName);
        return user;
    }

    @Override
    public Long createUser(User user) {
        User savedUser = repository.save(user);
        return savedUser.getId();
    }

    @Override
    public User updateUser(User user, Long id) {
        User currentUser = repository.findUserById(id);
        if (currentUser == null) {
            return null;
        }
        currentUser.setId(id);
        currentUser.setFirstName(user.getFirstName());
        currentUser.setLastName(user.getLastName());
        currentUser.setAge(user.getAge());
        currentUser.setEmail(user.getEmail());
        currentUser.setGender(user.getGender());
        currentUser.setBooks(new ArrayList<>(currentUser.getBooks()));
        repository.save(currentUser);
        return currentUser;
    }

    @Override
    public Long deleteUserById(Long id) {
        User user = repository.findUserById(id);
        if (user != null) {
            repository.delete(user);
            return id;
        }
        return null;
    }
}
