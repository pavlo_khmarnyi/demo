CREATE DATABASE IF NOT EXISTS users;

USE users;

CREATE TABLE IF NOT EXISTS user
(
    id         BIGINT AUTO_INCREMENT NOT NULL,
    first_name VARCHAR(30)           NOT NULL,
    last_name  VARCHAR(30)           NOT NULL,
    age        INT                   NOT NULL,
    email      VARCHAR(30)           NOT NULL,
    gender     VARCHAR(30)           NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS book
(
    id     BIGINT AUTO_INCREMENT NOT NULL,
    title  VARCHAR(30)           NOT NULL,
    author VARCHAR(30)           NOT NULL,
    genre  VARCHAR(30)           NOT NULL,
    user_id BIGINT,
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (user_id) REFERENCES user (id)
);
